//go:generate protoc --go_out=. --go_opt=paths=source_relative --go-grpc_out=. --go-grpc_opt=paths=source_relative -I. lb.proto
package proto

func (ns *NodeStatus) GetUtilizationByDimension(dim string) float64 {
	for _, u := range ns.Utilization {
		if u.Dimension == dim {
			return u.Value
		}
	}
	return 0
}
