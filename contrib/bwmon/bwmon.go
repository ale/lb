package bwmon

import (
	"bufio"
	"bytes"
	"errors"
	"os"
	"strconv"
)

const (
	RX = iota
	TX
)

// GetBandwidthUtilization returns a function that will report the
// current bytes counter for the specified interface and direction
// (transmit / receive), as read from /proc/net/dev.
func GetBandwidthUtilization(device string, direction int) func() float64 {
	return func() float64 {
		n, err := parseProcNetDev(device, direction)
		if err != nil {
			return 0
		}
		return float64(n)
	}
}

func parseProcNetDev(dev string, direction int) (uint64, error) {
	file, err := os.Open("/proc/net/dev")
	if err != nil {
		return 0, err
	}
	defer file.Close()

	// Device name (+ ':') in field 0
	devCol := []byte(dev + ":")

	// Receive bytes: field 1
	// Transmit bytes: field 9
	counterIndex := 1
	if direction == TX {
		counterIndex = 9
	}

	input := bufio.NewScanner(file)
	for input.Scan() {
		fields := bytes.Fields(input.Bytes())
		if bytes.Equal(fields[0], devCol) {
			n, err := strconv.ParseUint(string(fields[counterIndex]), 10, 64)
			if err != nil {
				return 0, err
			}
			return n, nil
		}
	}
	if err := input.Err(); err != nil {
		return 0, err
	}
	return 0, errors.New("device not found")
}
