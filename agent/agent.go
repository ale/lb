package lbagent

import (
	"context"
	"log"
	"sync"
	"sync/atomic"
	"time"

	"github.com/golang/protobuf/ptypes"

	lbpb "git.autistici.org/ale/lb/proto"
)

var defaultInterval = 5 * time.Second

type Dialer interface {
	Update(context.Context, *lbpb.NodeStatus) error
	Close()
}

type UtilFunc func() float64

type utilFuncWrapper struct {
	kind      lbpb.Utilization_Kind
	dimension string
	fn        UtilFunc
}

type Agent struct {
	dialer Dialer

	name      string
	tags      []*lbpb.Tag
	utilFuncs []utilFuncWrapper
	interval  time.Duration

	// Values that may be atomically modified from other goroutines.
	ready       uint32
	numRequests uint64

	// Internal synchronization.
	rootCtx context.Context
	cancel  context.CancelFunc
	wg      sync.WaitGroup
}

// New creates and returns a new Agent with the specified name and
// tags. The other parameters are required to establish a GRPC
// connection to the load balancer registry.
func New(name string, tags map[string]string, dialer Dialer) *Agent {
	// Using a top-level controlling Context allows us to cancel
	// in-flight update requests on Stop().
	ctx, cancel := context.WithCancel(context.Background())
	a := &Agent{
		name:     name,
		dialer:   dialer,
		rootCtx:  ctx,
		cancel:   cancel,
		interval: defaultInterval,
	}
	for k, v := range tags {
		a.tags = append(a.tags, &lbpb.Tag{Key: k, Value: v})
	}

	// Provide a built-in 'qps' utilization dimension using our
	// own request counter.
	a.RegisterUtilization("qps", lbpb.Utilization_COUNTER, func() float64 {
		return float64(atomic.LoadUint64(&a.numRequests))
	})

	return a
}

// Start the Agent in the background. Will return immediately.
func (a *Agent) Start() {
	a.wg.Add(1)
	go a.run()
}

// Stop the Agent.
func (a *Agent) Stop() {
	if a.cancel != nil {
		a.cancel()
	}
	a.wg.Wait()
	a.dialer.Close()
}

// SetUpdateInterval tells the Agent how often to send status
// updates. Must be called before Start().
func (a *Agent) SetUpdateInterval(interval time.Duration) {
	a.interval = interval
}

// RegisterUtilization registers an utilization callback for the
// specified dimension. Must be called before Start(). Registering
// multiple callbacks for the same dimension will produce
// unpredictable results.
func (a *Agent) RegisterUtilization(dimension string, kind lbpb.Utilization_Kind, fn UtilFunc) {
	a.utilFuncs = append(a.utilFuncs, utilFuncWrapper{
		dimension: dimension,
		kind:      kind,
		fn:        fn,
	})
}

// Inc increments the internal request counter by the specified
// amount.
func (a *Agent) Inc(n uint64) {
	atomic.AddUint64(&a.numRequests, n)
}

// SetReady tells the Agent if we're ready to serve or not.
func (a *Agent) SetReady(ready bool) {
	var value uint32
	if ready {
		value = 1
	}
	atomic.StoreUint32(&a.ready, value)
}

func (a *Agent) run() {
	defer a.wg.Done()

	tick := time.NewTicker(a.interval)
	defer tick.Stop()

	for {
		select {
		case <-a.rootCtx.Done():
			return
		case ts := <-tick.C:
			ctx, cancel := context.WithTimeout(a.rootCtx, a.interval)
			if err := a.update(ctx, ts); err != nil {
				log.Printf("update error: %v", err)
			}
			cancel()
		}
	}
}

func (a *Agent) update(ctx context.Context, timestamp time.Time) error {
	tsp, _ := ptypes.TimestampProto(timestamp)
	ns := &lbpb.NodeStatus{
		NodeName:    a.name,
		NumRequests: atomic.LoadUint64(&a.numRequests),
		Timestamp:   tsp,
		Tags:        a.tags,
	}
	if ready := atomic.LoadUint32(&a.ready); ready == 1 {
		ns.Ready = true
	}
	for _, ufn := range a.utilFuncs {
		ns.Utilization = append(ns.Utilization, &lbpb.Utilization{
			Kind:      ufn.kind,
			Dimension: ufn.dimension,
			Value:     ufn.fn(),
		})
	}
	return a.dialer.Update(ctx, ns)
}
